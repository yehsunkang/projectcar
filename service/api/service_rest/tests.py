from django.test import TestCase, Client
from django.urls import reverse
import json

class TestServicesList(TestCase):
    def setUp(self):
        self.client = Client()

    def test_services_list_GET(self):
        # Arrange
        url = reverse('api_list_services')
        
        # Act
        response = self.client.get(url)
        
        # Assert
        self.assertEqual(response.status_code, 200)
        self.assertTrue('services' in response.json())
