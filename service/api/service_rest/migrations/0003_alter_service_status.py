# Generated by Django 4.0.3 on 2024-02-16 00:28

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('service_rest', '0002_alter_service_status'),
    ]

    operations = [
        migrations.AlterField(
            model_name='service',
            name='status',
            field=models.ForeignKey(default=1, null=True, on_delete=django.db.models.deletion.PROTECT, related_name='services', to='service_rest.status'),
        ),
    ]
