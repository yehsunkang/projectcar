from common.json import ModelEncoder
from .models import AutomobileVO, Service, Technician, Status


class StatusEncoder(ModelEncoder):
    model = Status
    properties = ["name","id"]


class TechnicianEncoder(ModelEncoder):
    model = Technician
    properties = [
        # "id",
        "name",
        "employee_number",
    ]


class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "vin",
        "color",
        "year",
        "model",  
    ]


class ServiceEncoder(ModelEncoder):
    model = Service
    properties = [
        "vin",
        "customer_name",
        "date",
        "time",
        "reason",
        "technician",
        "status",
    ]
    encoders = {
        "status": StatusEncoder(),
        "technician": TechnicianEncoder(),
    }

    def get_extra_data(self, o):
        count = AutomobileVO.objects.filter(vin=o.vin).count()
        return {
            "vip": count > 0,
            "status": {
                "name": o.status.name,
                "id": o.status.id
            }
        }