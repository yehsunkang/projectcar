import React, { useState } from "react";
// import { Navigate } from "react-router-dom";
import { useToken } from "./Auth";
import { Link } from 'react-router-dom';

function Signup(props) {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [email, setEmail] = useState("");
  const [,,,signup] = useToken();


  var handleUserName = function (e) {
    const value = e.target.value;
    setUsername(value);
  };

  return (
    <div className="shadow p-4 mt-4 login-signup ">
    <section className="vh-100">
      <p></p>
      
      <div className="container-fluid h-custom">
        <div className="row d-flex justify-content-center align-items-center h-100">
          <div className="col-md-8 col-lg-6 col-xl-4 offset-xl-1">
            <br />
            <form>
            <div className="d-flex flex-row align-items-center justify-content-center justify-content-lg-start">
            <h4 style={{ fontWeight: "bold" }}> Sign Up </h4>
              </div>
              <div className="form-outline mb-3">
                <input
                  onChange={handleUserName}
                  required
                  type="text"
                  id="username"
                  className="form-control"
                  placeholder="Username"
                  value={username}
                />
              </div>
              <div className="form-outline mb-3">
                <input
                  onChange={(e) => setPassword(e.target.value)}
                  required
                  type="password"
                  id="password"
                  className="form-control"
                  placeholder="Password"
                  value={password}
                />
              </div>
              <div className="form-outline mb-3">
                <input
                  onChange={(e) => setEmail(e.target.value)}
                  required
                  type="email"
                  id="email"
                  className="form-control"
                  placeholder="Email"
                  value={email}
                />
              </div>
              <div className="text-center text-lg-start pt-2">
                <button
                  type="button"
                  className="btn btn-success"
                  style={{
                    paddingLeft: "1.5rem",
                    paddingRight: "1.5rem",
                    paddingBottom: ".5rem",
                  }}
                  onClick={() => signup(username, password, email)}
                >
                  Sign Up
                </button>
                
              </div>
            </form>
              <div className='mt-2'>
                  Already have an account? 
                    <Link to="/users/login" className="login-signup-link-text-color">
                     Sign in here
                    </Link>
              </div>
          </div>
        </div>
      </div>
    </section>
    </div>
  );
}

export default Signup;
