import React, { useState } from "react";
// import { Navigate } from "react-router-dom";
import { useToken } from "./Auth";
// import { useAuthContext } from "./Auth";
import { Link } from 'react-router-dom';

function Login(props) {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [,login] = useToken();


  var handleUserName = function (e) {
    const value = e.target.value;
    setUsername(value);
  };

  return (
    <div className="shadow p-4 mt-4 login-signup ">
    <section className="vh-100">
      <div className="container-fluid h-custom">
        <div className="row d-flex justify-content-center align-items-center h-100">
          <div className="col-md-8 col-lg-6 col-xl-4 offset-xl-1">
            <br />
            <form>
              <div className="d-flex flex-row align-items-center justify-content-center justify-content-lg-start">
              <h4 style={{ fontWeight: "bold" }}> Log In </h4>
              </div>
              <div className="form-outline mb-3">
                <input
                  onChange={handleUserName}
                  required
                  type="text"
                  id="username"
                  className="form-control"
                  placeholder="Username"
                  value={username}
                />
              </div>
              <div className="form-outline mb-3">
                <input
                  onChange={(e) => setPassword(e.target.value)}
                  required
                  type="password"
                  id="password"
                  className="form-control"
                  placeholder="Password"
                  value={password}
                />
              </div>
              <div className="text-center text-lg-start mt-4 pt-2">
                <button
                  type="button"
                  className="btn btn-success"
                  style={{
                    paddingLeft: "1.5rem",
                    paddingRight: "1.5rem",
                    paddingBottom: ".5rem",
                  }}
                  onClick={() => login(username, password)}
                >
                  Log In
                </button>
              </div>
            </form>
            <div className='mt-2'>
                Don't have an account? 
                <Link to="/users/signup" className="login-signup-link-text-color">
                    Sign up here
                </Link>
            </div>
          </div>
        </div>
      </div>
    </section>
    </div>
  );
}

export default Login;
