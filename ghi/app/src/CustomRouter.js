import React, { useState, useEffect } from 'react';
import { Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import ServiceForm from './Services/ServiceForm';
import TechnicianForm from './Services/TechnicianForm';
import ServiceHistory from './Services/ServiceHistory';
import { useToken } from "./users/Auth";
import Signup from './users/Signup';
import Login from "./users/Login";
import Logout from "./users/Logout";
import { jwtDecode } from "jwt-decode";

function CustomRouter() {

  const [token, login, logout, signup] = useToken()
  const [data, setData] = useState({ user: { perms: [] } })

  useEffect(() => {
    if (!!token) {
      setData(jwtDecode(token));
    } else {
      setData({ user: { perms: [] } });
    }
  }, [token]);

  return (
    <>
      <Nav userId={data.user.id} perms={data.user.perms}/>
      <Routes>
        <Route path="/" element={<MainPage props={{ userId: data.user.id }} />} />
          <Route path="users">
            <Route path="signup" element={<Signup signup={signup} />} />
            <Route path="login" element={<Login login={login} />} />
            <Route path="logout" element={<Logout logout={logout} />} />
          </Route>
          <Route path="services">
            {data.user.perms.includes("auth.add_permission") ? (
              <>
                <Route path="new" element={<ServiceForm />} />
                <Route path="history" element={<ServiceHistory />} />
              </>
            ) : (
              <>
                <Route path="new" element={<ServiceForm />} />
              </>
            )}
          <Route path="technician/new" element={<TechnicianForm />} />
        </Route>
      </Routes>
    </>
  );
}
export default CustomRouter;