import React from "react"
import { BrowserRouter } from 'react-router-dom'
import { AuthProvider } from "./users/Auth"
import CustomRouter from "./CustomRouter"

function App() {
  const domain = /https:\/\/[^/]+/;
  const basename = process.env.PUBLIC_URL.replace(domain, '');

  return (
    <AuthProvider>
      <BrowserRouter basename={basename}>
        <div id="outer-div" className="d-flex flex-column vh-100 vw-96">
          <CustomRouter />
        </div>
      </BrowserRouter>
    </AuthProvider>
  );
}
export default App;
