import React, { useState, useEffect } from 'react';
import TechnicianForm from './TechnicianForm';

const ServiceForm = () => {
    const [showSuccessMessage, setShowSuccessMessage] = useState(false);
    const [showTechnicianForm, setShowTechnicianForm] = useState(false);
    const [state, setState] = useState({
        vin: '',
        customer_name: '',
        date: '',
        time: '',
        reason: '',
        technician: '',
        technicians: [],
    });
    
    const handleToggleTechnicianForm = () => {
        setShowTechnicianForm(!showTechnicianForm);
    };

    const handleChangeVin = (event) => {
        const value = event.target.value;
        setState({ ...state, vin: value });
    };

    const handleChangeCustomerName = (event) => {
        const value = event.target.value;
        setState({ ...state, customer_name: value });
    };

    const handleChangeDate = (event) => {
        const value = event.target.value;
        setState({ ...state, date: value });
    };

    const handleChangeTime = (event) => {
        const value = event.target.value;
        setState({ ...state, time: value });
    };

    const handleChangeReason = (event) => {
        const value = event.target.value;
        setState({ ...state, reason: value });
    };

    const handleChangeTechnician = (event) => {
        const value = event.target.value;
        setState({ ...state, technician: value });
    };

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = { ...state };
        delete data.technicians;
        try {
        const serviceUrl = 'http://localhost:8080/api/services/';
        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(serviceUrl, fetchConfig);
        if (response.ok) {
            const newservice = await response.json();
            console.log(newservice);

            setShowSuccessMessage(true);
            setTimeout(() => {
                setState({
                    vin: '',
                    customer_name: '',
                    date: '',
                    time: '',
                    reason: '',
                    technician: '',
                });
                setShowSuccessMessage(false);
                window.location.reload();
            }, 5000);      
        } else {
            throw new Error(`HTTP error! status: ${response.status}`);
          }
            
        } catch (error) {
            console.error('Error submitting data:', error);
            alert(`An error occurred: ${error.message}`);
         }
     };

    useEffect(() => {
        const fetchData = async () => {
            try {
                const url = 'http://localhost:8080/api/technician/';
                const response = await fetch(url);
                if (response.ok) {
                    const data = await response.json();
                    setState((prevState) => ({ ...prevState, technicians: data.technicians }));
                } else {
                    throw new Error(`HTTP error! status: ${response.status}`);
                }
            } catch (error) {
                console.error('Error fetching conferences:', error);
            }
        };
        fetchData();
    }, []);

    const spinnerClasses = state.technicians.length > 0 ? 'd-flex justify-content-center mb-3 d-none' : 'd-flex justify-content-center mb-3';
    const dropdownClasses = state.technicians.length > 0 ? 'form-select' : 'form-select d-none';
  
    const messageClasses = showSuccessMessage ? 'alert alert-success mb-0' : 'alert alert-success d-none mb-0';
    const formClasses = showSuccessMessage ? 'd-none' : '';
  
    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a appointment</h1>
                    <br />
                    <div className={spinnerClasses} id="loading-appointment-spinner">
                  <div className="spinner-grow text-secondary" role="status">
                    <span className="visually-hidden">Loading...</span>
                  </div>
                </div>
                {!showSuccessMessage && (
                 <>
                    <form className={formClasses} onSubmit={handleSubmit} id="create-service-form">
                        <div className="form-floating mb-3">
                            <input
                                onChange={handleChangeVin}
                                required
                                placeholder="VIN"
                                type="text"
                                id="vin"
                                name="vin"
                                className="form-control"
                                value={state.vin}
                            />
                            <label htmlFor="vin">VIN</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input
                                onChange={handleChangeCustomerName}
                                required
                                placeholder="Customer Name"
                                type="text"
                                id="customer_name"
                                name="customer_name"
                                className="form-control"
                                value={state.customer_name}
                            />
                            <label htmlFor="customer_name">Customer Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input
                                onChange={handleChangeDate}
                                required
                                placeholder="Date"
                                type="date"
                                id="date"
                                name="date"
                                className="form-control"
                                value={state.date}
                            />
                            <label htmlFor="date">Date</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input
                                onChange={handleChangeTime}
                                required
                                placeholder="Time"
                                type="time"
                                id="time"
                                name="time"
                                className="form-control"
                                value={state.time}
                            />
                            <label htmlFor="time">Time</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input
                                onChange={handleChangeReason}
                                required
                                placeholder="Reason"
                                type="text"
                                id="reason"
                                name="reason"
                                className="form-control"
                                value={state.reason}
                            />
                            <label htmlFor="reason">Reason</label>
                        </div>
                        <div className="mb-3">
                            <select
                                onChange={handleChangeTechnician}
                                required
                                id="technician"
                                name="technician"
                                className={dropdownClasses} 
                                value={state.technician}
                            >
                                <option>Choose a technician</option>
                                {state.technicians.map((technician) => (
                                    <option 
                                    key={technician.employee_number} 
                                    value={technician.employee_number}>
                                        {technician.name}
                                    </option>
                                ))}
                            </select>
                        </div>            
                        <button className="btn btn-lg btn-warning">Create</button>
                    </form>                 
                <div className="mt-4">
                        <button 
                            className="btn btn-dark" 
                            onClick={handleToggleTechnicianForm}
                            >
                            Add technician
                        </button>
                    </div>
              {showTechnicianForm && !showSuccessMessage && <TechnicianForm />}
            </>
            )}
                <div className={messageClasses} id="success-message">
                Appointment for <strong>{state.customer_name}</strong> has been successfully created!<br /> 
                Vehicle VIN: <strong>{state.vin}</strong><br /> 
                Date: <strong>{state.date}</strong><br /> 
                
                </div>
              </div>
            </div>
        </div>
    );
};

export default ServiceForm;