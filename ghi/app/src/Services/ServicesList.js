import React, { useState, useEffect } from 'react';

function ServicesList() {
    const [services, setServices] = useState([]);

    useEffect(() => {
        async function fetchData() {
            const url = 'http://localhost:8080/api/services/';
            const response = await fetch(url);
            if (response.ok) {
                const data = await response.json();
                setServices(data.services);
            }
        }
        fetchData();
    }, []);

    const handleChangeStatus = async (service, status) => {
        console.log("Button is clicked");
        const data = { "status": status };
        const serviceUrl = `http://localhost:8080/api/services/${service.vin}/status/`;
        const fetchConfig = {
            method: "PUT",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(serviceUrl, fetchConfig);
        if (response.ok) {
            const newStatus = await response.json();
            console.log(newStatus);
            window.location.reload();
        }
    };

    return (
        <>
         
            <p></p>
            <h1> Service Appointment</h1>
            <form>
                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th>VIN</th>
                            <th>Customer Name</th>
                            <th>Date</th>
                            <th>Time</th>
                            <th>Technician</th>
                            <th>Reason</th>                        
                            <th>Status</th>
                        </tr>
                    </thead>
                    <tbody>
                        {services.map(service => {
                            if (service.status.id === 1) {
                                return (
                                    <tr key={service.vin}>
                                        <td>{service.vin}</td>
                                        <td>{service.customer_name}</td>
                                        <td>{service.date.split("T")[0]}</td>
                                        <td>{service.time.split("T")[1].slice(0, 5)}</td>
                                        <td>{service.technician.name}</td>
                                        <td>{service.reason}</td>                                       
                                        <td>
                                            <button onClick={() => handleChangeStatus(service, "Cancel")} 
                                            type="button" 
                                            className="btn btn-warning btn-sm"
                                            style={{ marginRight: '5px' }}>Cancel</button>
                                            <button onClick={() => handleChangeStatus(service, "Finish")} 
                                            type="button" 
                                            className="btn btn-dark btn-sm">Finish</button>
                                        </td>
                                    </tr>
                                );
                            }
                            return null;
                        })}
                    </tbody>
                </table>
            </form>
           
        </>
    );
}

export default ServicesList;