import React, { useState } from 'react';

const TechnicianForm = () => {
    const [name, setName] = useState('');
    const [employeeNumber, setEmployeeNumber] = useState('');

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = { name, employee_number: employeeNumber };
        const technicianUrl = 'http://localhost:8080/api/technician/';
        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(technicianUrl, fetchConfig);
        if (response.ok) {
            const newTechnician = await response.json();
            console.log(newTechnician);
            setName('');
            setEmployeeNumber('');
        }
        window.location.reload();
    };

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <form onSubmit={handleSubmit} id="create-technician-form">
                        <div className="form-floating mb-3">
                            <input
                                value={name}
                                onChange={(e) => setName(e.target.value)}
                                placeholder="Name"
                                required
                                type="text"
                                name="name"
                                id="name"
                                className="form-control"
                            />
                            <label htmlFor="name">Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input
                                value={employeeNumber}
                                onChange={(e) => setEmployeeNumber(e.target.value)}
                                placeholder="Employee Number"
                                required
                                type="text"
                                name="employee_number"
                                id="employee_number"
                                className="form-control"
                            />
                            <label htmlFor="employee_number">Employee Number</label>
                        </div>
                        <button className="btn btn-warning">Create</button>
                    </form>
                </div>
            </div>
        </div>
    );
};

export default TechnicianForm;