import React, { useState } from 'react';
import ServicesList from './ServicesList';


const ServiceHistory = () => {
    const [vin, setVin] = useState('');
    const [services, setServices] = useState([]);
    const [searchPerformed, setsearchPerformed] = useState(false);

    const handleChangeVin = (event) => {
        const value = event.target.value;
        setVin(value);
    };

    const onSearch = async (event) => {
        event.preventDefault();
        setsearchPerformed(true);
        const data = { vin };
        console.log(data);

        const vinUrl = `http://localhost:8080/api/services/${data.vin}`;
        const response = await fetch(vinUrl);
        if (response.ok) {
            const vins = await response.json();
            console.log(vins);
            setServices(Array.isArray(vins) ? vins : [vins]);
        } else {
            setServices([]); 
        }
        setVin('');     
    };
    
const formatDate = (isoString) => {
    const date = new Date(isoString);
    return date.toISOString().substring(0, 10); 
};

const formatTime = (isoString) => {
    const date = new Date(isoString);
    return `${date.getHours().toString().padStart(2, '0')}:${date.getMinutes().toString().padStart(2, '0')}`; 
};

     return (
        <>
        <div className="container-fluid navbar-padding">
            <br />
                <div className="container-fluid navbar-padding">
                    <h1> Service History</h1>
                    <p></p>
                    <div>
                        <form onSubmit={onSearch} id="search-bar" className="search-bar">
                            <div className="input-group">
                                <input
                                    onChange={handleChangeVin}
                                    value={vin}
                                    required
                                    placeholder="Enter VIN"
                                    type="search"
                                    id="search"
                                    name="vin"
                                    className="form-control rounded "
                                />
                                <button className="btn btn-warning"> Search</button>
                            </div>
                        </form>
                    </div>
                </div>
            <div className="service list navbar-padding">
                <p></p>
                {searchPerformed && services.length === 0 && (
                    <p>[ No record found ]</p>
                )}
                {services.length > 0 && (
                    <table className="table table-striped ">
                        <tbody>
                            {services.map((service, index) => (
                                <tr key={index}>
                                    <td>{service.vin}</td>
                                    <td>{service.customer_name}</td>
                                    <td>{formatDate(service.date)}</td> 
                                    <td>{formatTime(service.time)}</td>
                                    <td>{service.technician.name}</td>
                                    <td>{service.reason}</td>
                                </tr>
                            ))}
                        </tbody>
                    </table>
                )}
            </div>
            <br />
                <div className="container-fluid navbar-padding">
                <ServicesList />
                </div>
            </div>
        </>
    );
};

export default ServiceHistory;