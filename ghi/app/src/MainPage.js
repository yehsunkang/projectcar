import React from "react";
import { Link } from 'react-router-dom';
import "./index.css";

function MainPage({ props }) {
  return (
    <div className="px-2 py-2 my-2 text-center bg-light">
      <h1 className="display-5 fw-bold">CarCar</h1>
      <div className="col-lg-6 mx-auto">
        <p className="lead mb-4">
          The premiere solution for automobile maintenance management!
        </p>
        <img className="bg-white rounded shadow d-block mx-auto mb-4" src="./carmain.jpg" alt="" width="600" />
          {props.userId ? (
            <Link to="/services/new" className="btn btn-dark hover-btn">Make an appointment</Link>
          ) : (
            <Link to="/users/signup" className="btn btn-dark hover-btn">Make an appointment</Link>
          )}
      </div>
    </div>  
  );
}

export default MainPage;