import React from "react";
import { NavLink } from "react-router-dom";
import "./index.css";

function Nav(props) {
  const isRegularUser = !props.perms.includes("auth.add_permission");
  const isSuperUser = props.perms.includes("auth.add_permission");

  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
      <div className="container-fluid navbar-padding">
        <NavLink className="navbar-brand" to="/">
          CarCar
        </NavLink>
        <button
          className="navbar-toggler"
          type="button"
          data-bs-toggle="collapse"
          data-bs-target="#navbarSupportedContent"
          aria-controls="navbarSupportedContent"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <div className="navbar" id="navbarCollapse">
              <NavLink className="nav-link active" aria-current="page" to="/">
                Home
              </NavLink>
              {!!props.userId && (
                <li className="nav-item dropdown">
                  <a
                      className="nav-link dropdown-toggle"
                      href="/"
                      id="navbarDropdown"
                      role="button"
                      data-bs-toggle="dropdown"
                      aria-expanded="false"
                    >
                      Services
                  </a>
                  <ul className="dropdown-menu" aria-labelledby="navbarDropdown">
                    {isRegularUser && ( 
                      <NavLink
                        className="dropdown-item"
                        aria-current="page"
                        to="/services/new/"
                      >
                        Create a Service Appointment
                      </NavLink>
                    )}
                    {isSuperUser && (
                      <>
                      <NavLink
                        className="dropdown-item"
                        aria-current="page"
                        to="/services/history"
                      >
                        Service History
                      </NavLink>
                      <NavLink
                        className="dropdown-item"
                        aria-current="page"
                        to="/services/new/"
                      >
                        Create a Service Appointment
                      </NavLink>
                      </>
                    )}
                  </ul>
                </li>
              )}
            </div>
          </ul>
          {!!props.userId ? (
            <ul className="navbar-nav mb-2 mb-lg-0">
              <li className="nav">
                <NavLink className="btn rounded btn-outline-light me-2" to="/users/logout">
                  Log Out
                </NavLink>
              </li>
            </ul>
          ) : (
            <ul className="navbar-nav mb-2 mb-lg-0">
              <li className="nav">
                <NavLink className="btn rounded btn-outline-light me-2" to="/users/login">
                  Log In
                </NavLink>
              </li>
              <li className="nav">
                <NavLink className=" btn rounded btn-outline-light me-2" to="/users/signup">
                  Sign Up
                </NavLink>
              </li>
            </ul>
          )}
        </div>
      </div>
    </nav>
  );
}

export default Nav;