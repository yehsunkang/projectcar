# CarCar

A CRUD app that allows service dealers to track record of services,and customers to create a service request

## Technologies Used

    - Python
    - JavaScript
    - PostgreSQL
    - React
    - Node.js
    - Django
    - Docker
    - RESTful API's


# About the project
The single-page web application consists of two microservices, user and service, facilitate creating service appointment for clients and managing service records for service shops. 
* The User microservice:
  * Handles user registration, authentication, and authorization processes. It leverages Django's built-in AbstractUser model for user management, extending its capabilities to suit the specific needs of the application. 
  * It ensures that user management is streamlined and secure, employing JSON Web Tokens (JWT) for secure communication.
  
* The Service microservice:
  * Through its API endpoints, it offers functionality for listing, creating, updating, and deleting service appointments, as well as managing service statuses and technician details.
  * This microservice is pivotal in enabling efficient service shop operations, ensuring a smooth workflow from scheduling services to their completion.


## Home
It dynamically displays a link for either signing up or making an appointment based on the user's login status. This component combines informational content with user interaction, highlighted by an inviting design that guides users towards their next step in automobile maintenance management.

![home page](docs/wireframes/main.png)


## Authentication
The Nav function component in React renders a navigation bar for the CarCar application, utilizing conditional rendering to display different options based on user permissions and login status. It differentiates between regular and super users, providing access to specific features like creating a service appointment for regular users, and additional options like viewing service history for super users. The component integrates responsive design elements for accessibility and user-friendliness, ensuring a seamless navigation experience across different devices.

![detail page](docs/wireframes/login.png)


## Service Appointment List
The ServicesList component in React fetches and displays a list of service appointments from a backend API upon component mount, using the useEffect hook to perform the fetch operation asynchronously. It provides an interface for users to change the status of a service appointment directly from the list, supporting operations to either cancel or finish an appointment through the handleChangeStatus function, which sends a PUT request to update the service status. This component effectively combines data fetching, state management, and user interactions to allow for dynamic updates to the service appointments, enhancing the functionality and user experience of the application.

## Service History
The ServiceHistory component in React allows users to search for service records by Vehicle Identification Number (VIN) and displays a list of matching service entries. It features an input form for VIN searches, which triggers an asynchronous fetch request to retrieve and display service history from a backend service upon submission. The component also includes formatting functions for dates and times to enhance data presentation, and conditionally renders service records or a no record found message based on the search results, demonstrating a thoughtful user interface design for historical data retrieval.

![profile page](docs/wireframes/appointmentList.png)


## Create a service appointment
The ServiceForm component in React facilitates the creation of service appointments through a user-friendly form, enabling input for vehicle identification number (VIN), customer name, date, time, reason for the appointment, and selection of a technician from a dynamically populated dropdown. It incorporates a toggleable TechnicianForm for adding new technicians, showcases conditional rendering to display a success message post-submission, and uses useState and useEffect hooks for managing state and fetching technician data, respectively. 

![profile page](docs/wireframes/appointmentForm.png)

## Success Message
This component enhances user interaction by offering immediate feedback upon successful form submission and refreshing the form for new entries, streamlining the process of scheduling service appointments.

![profile page](docs/wireframes/success.png)