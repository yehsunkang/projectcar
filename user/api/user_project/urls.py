"""users_project URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from user_rest.views import (
    get_users,
    create_user,
    delete_user,
    update_user,
)
from django.http import HttpResponse

def home(request):
    return HttpResponse("Welcome to the Users_API!!!???$$$")


urlpatterns = [
    path('', home, name='home'),
    path("admin/", admin.site.urls),
    path("", include("djwto.urls")),
    path("users/", get_users, name="get_users"),
    path("users/new/", create_user, name="create_user"),
    path("users/delete/<int:pk>/", delete_user, name="delete_user"),
    path("users/update/<int:pk>/", update_user, name="update_user"),
    path("api/", include("user_rest.urls")),
]
