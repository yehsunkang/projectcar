from django.core.management.base import BaseCommand
from user_rest.models import User


class Command(BaseCommand):
    def handle(self, *args, **options):
        username = "admin"
        email = "admin@admin.com"
        password = "admin"

        if not User.objects.filter(username=username).exists():
            print("Creating account for %s (%s)" % (username, email))
            User.objects.create_superuser(
                email=email, username=username, password=password
            )
        else:
            print("Admin account has already been initialized.")
