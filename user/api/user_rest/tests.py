from django.test import TestCase, Client


class TestUsers(TestCase):
    def test_successfully_get_users(self):
        # Arrange
        c = Client()
        # Act
        response = c.get("/users/")
        result = response.content
        # Assert
        self.assertIsNotNone(result)
