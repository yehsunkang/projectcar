# Generated by Django 4.0.3 on 2024-02-16 05:02

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('user_rest', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='user',
            name='bio',
        ),
    ]
